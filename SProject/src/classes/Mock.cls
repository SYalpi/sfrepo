@isTest
global class Mock implements HttpCalloutMock{
    global HTTPResponse respond(HTTPRequest request){
        
        HttpResponse res=new HttpResponse();
        res.setStatusCode(200);
        return res;
    }
}