public class listOfOpportunities {
    
    list<opportunity> myOpps{set;get;}
    
    public void listOfWonOpps(){
        List<opportunity> myOpps=[select name from opportunity where StageName='Closed Won'];
        for(opportunity o:myOpps){
            system.debug(o.name);
        }
    }
    
    public void listOfOppsInSpecifiedRange(Date ClosedDateStart,Date closeDateEnd){
        myopps=[select name from opportunity where 	CloseDate>=:ClosedDateStart and CloseDate<=:closeDateEnd];
        for(opportunity o:myOpps){
            system.debug(o.name);
        }
    }
    
    public void listofOppsBySalesRep(String ownerName){
        myopps=[select name from opportunity where 	Owner.lastname=:ownerName];
        for(opportunity o:myOpps){
            system.debug(o.name);
        }
    }
    
    
}



/*listOfOpportunities lop=new listOfOpportunities();

Date myDate1 = Date.newInstance(1960, 2, 17);
Date myDate2 = Date.newInstance(2022, 2, 17);

lop.listofOppsBySalesRep('Yalpi');*/