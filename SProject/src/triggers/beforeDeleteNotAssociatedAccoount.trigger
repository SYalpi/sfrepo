trigger beforeDeleteNotAssociatedAccoount on Contact (before delete) {
	list<contact> contacts=trigger.old;
    for(contact c:contacts){
        if(c.account.id==null){
            c.addError('contact not associated with account');
        }
    }
}